import os
import typer

from pathlib import Path
from database import Shelf
from utils import die, Project

from icecream import ic


app = typer.Typer()
project_subcommand = typer.Typer()
app.add_typer(project_subcommand, name='project')

entries: set[Project] = []


@project_subcommand.command('add')
def project_add(
    path: Path,
    name: str = None,
    runner: str = None,
    editor: str = 'nvim',
    multi: bool = False
):

    global entries

    if multi:
        die('Cannot specify name for a dir', 2) if name else None

        for dir in os.listdir(path):

            entry = create_entry(path/dir, dir, runner, editor, True)

            ic(path/dir)
            entries.append(entry)

    else:
        entry = create_entry(path.resolve(), name, runner, editor, False)
        ic(entry)
        entries.append(entry)

    with Shelf('spyglass') as shelf:
        shelf.update(entries)


@project_subcommand.command('remove')
def project_remove(path: Path, name: str = None, multi: bool = False):

    global entries
    #
    # if multi:
    #     die('Cannot specify name for a dir', 2) if name else None
    #
    #     for dir in os.listdir(path):
    #         entries['dirs'].append(create_entry(path/dir, name, runner, editor))
    #
    # else:
    #     entries['projects'].append(create_entry(path, name, runner, editor))
    #
    # with Shelf('spyglass') as shelf:
    #     shelf.update(entries)
    #


@project_subcommand.command('list')
def project_list():

    with Shelf('spyglass') as shelf:
        shelf.list()



def create_entry(path: Path, name: str, runner: str, editor: str, multi: bool) -> Project:

    proj: Project = Project(
        path,
        name if name else os.path.dirname(path),
        str(path/runner) if runner else None,
        editor if editor else 'nvim',
        multi
    )

    return proj



def main():
    app()


if __name__ == '__main__':
    main()
